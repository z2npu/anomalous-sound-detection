# anomalous-sound-detection

#### 介绍
mobileNetV1预训练模型。来自[PANNs](https://github.com/qiuqiangkong/audioset_tagging_cnn)，在超过5000小时的audioset数据集上进行了预训练。

#### 安装说明

1. 安装pytorch
2. 安装librosa
3. 安装torchlibrosa，建议通过pip语句安装 `pip install torchlibrosa`